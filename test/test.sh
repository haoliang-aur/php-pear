#!/usr/bin/env bash

ROOT=$(dirname $(realpath $0))
IMAGE="haoliang-aur/php-pear"

main() {

    cp -f $ROOT/../PKGBUILD ./

    docker build -t $IMAGE . || {
        >&2 echo "build image failed"
        return 1
    }

    docker run --rm -it -w /php-pear --entrypoint "" $IMAGE makepkg -sirc --noconfirm || {
        >&2 echo "install package php-pear failed"
        return 1
    }
}

cd $ROOT && main
